
Pod::Spec.new do |s|
    s.name             = "KasaDateIntervalPicker"
    s.version          = "1.0.0"
    s.summary          = "KasaDateIntervalPicker"
    s.homepage         = "http://www.mobilkasa.com"
    s.license      = { :type => 'Apache License, Version 2.0', :text => <<-LICENSE
        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and
        limitations under the License.
        LICENSE
    }

    s.author           = { "Ismail Bozkurt" => "ismail@mobilkasa.com" }
    s.source           = { :git => "https://bitbucket.org/kasadev/dateintervalpicker.git", :tag => s.version.to_s }
    s.social_media_url = 'https://twitter.com/mobilkasa'

    s.platform     = :ios, '8.0'
    s.requires_arc = true
#    s.ios.deployment_target = '8.0'

    s.source_files = 'DateIntervalPicker/DateIntervalPickerView/*.{swift}'
    
#    s.frameworks = 'UIKit'

    s.dependency 'IBViewBorders'
#    s.dependency 'KasaCalendarView'
    #, :podspec => 'https://bitbucket.org/kasadev/specs/raw/master/KasaCalendarView/KasaCalendarView.podspec'

end

